/*$************************************
 * \file   StaticProto.cc             *
 * \date   June 2019                  *
 * \author Sofian Touhami             *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 **************************************/

#include <StaticProto.h>

/*!**********************************************************************************************
 * class StaticMessage                                                                          *
 *     @::BuildFromFile           : Populates the inner protobuf message with the file's data   *
 *     @::BuildFromString         : Populates the inner protobuf message with the string's data *
 *     @::SerializeToFile         : Encodes the message into a file                             *
 *     @::SerializeToOutputStream : Encodes the message into a ostream                          *
 *     @::SerializeToString       : Encodes the message into a string                           *
 *     @::ToString                : Helper function, returns the SerializeToString result       *
 *     @::Clear                   : Clears the inner protobuf message                           *
 ************************************************************************************************/

/*@*****************
 * ::BuildFromFile *
 *******************/
bool StaticMessage::BuildFromFile(std::string const& filepath)
{
    std::fstream input(filepath, std::ios::in | std::ios::binary);
    if (input)
        return m_message->ParseFromIstream(&input);
    return false;
}

/*@*******************
 * ::BuildFromString *
 *********************/
bool StaticMessage::BuildFromString(std::string const& encodedstring)
{
    return m_message->ParseFromString(encodedstring);
}

/*@********************
 * ::SerialiazeToFile *
 **********************/
bool StaticMessage::SerialiazeToFile(std::string const& filepath)
{
    std::fstream output(filepath, std::ios::out | std::ios::trunc | std::ios::binary);
    return m_message->SerializeToOstream(&output);
}

/*@****************************
 * ::SerialiazeToOutputStream *
 ******************************/
bool StaticMessage::SerialiazeToOutputStream(std::ostream & ostream)
{
    return m_message->SerializeToOstream(&ostream);
}

/*@*********************
 * ::SerializeToString *
 ***********************/
bool StaticMessage::SerializeToString(std::string & ostring)
{
    return m_message->SerializeToString(&ostring);
}

/*@************
 * ::ToString *
 **************/
std::string const StaticMessage::ToString()
{
    std::string encoded_str;
    this->SerializeToString(encoded_str);

    return encoded_str;
}

/*@*********
 * ::Clear *
 ***********/
void StaticMessage::Clear()
{
    m_message->Clear();
}

