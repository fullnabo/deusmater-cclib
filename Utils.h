#pragma once

/*$******************************************************
 * \file   Utils.h                                      *
 * \date   June 2019                                    *
 * \author Sofian Touhami                               *
 *                                                      *
 * Contact: sofian.engineer@gmail.com                   *
 *                                                      *
 * \brief Defines helper functions                      *
 ********************************************************/

/*@*************************************************************
 * destroy : Makes a delete                                    *
 *         , Ensures that it is a lvalue                       *
 *         , Ensures that it is left to nullptr after deletion *
 *                                                             *
 * \brief Avoid crash due to multiple calls to delete          *
 ***************************************************************/
template
    <class T> 
inline 
void destroy(T*& p)
{ 
    delete p; 
    p = nullptr; 
}

/*!************************************************
 * \class   DisableCopy                           *
 * \author  Sofian Touhami                        *
 * \version 1.0                                   *
 * \date    June 2019                             *
 *                                                *
 * Contact: sofian.engineer@gmail.com             *
 *                                                *
 *                                                *
 * \brief Allows quick "Rule of five" acceptance  *
 *        , in case only the destructor is needed *
 *                                                *
 * Deactivate move and copy                       *
 *                                                *
 * \note Rule of five :                           *
 *       If one in                                *
 *           -> destructor                        *
 *           -> copy constructor                  *
 *           -> move constructor                  *
 *           -> copy asignment                    *
 *           -> move asignment                    *
 *       is defined, the others has to            *
 **************************************************/
class DisableCopy
{
protected:
   DisableCopy() = default;
  ~DisableCopy() = default;

private:
  DisableCopy                 (DisableCopy const&) = delete;
  DisableCopy const& operator=(DisableCopy const&) = delete;
};