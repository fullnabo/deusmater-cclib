#pragma once

/*$*******************************************************************
 * \file   StaticProto.h                                             *
 * \date   June 2019                                                 *
 * \author Sofian Touhami                                            *
 *                                                                   *
 * Contact: sofian.engineer@gmail.com                                *
 *                                                                   *
 * \brief Base class implementing common protobuf message operations *
 *********************************************************************/

#include <google/protobuf/message.h>

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

/*!************************************
 * \class   StaticMessage             *
 * \author  Sofian Touhami            *
 * \version 1.0                       *
 * \date    June 2019                 *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 *                                    *
 **************************************/
class StaticMessage
{
public:
    bool BuildFromFile   (std::string const& filepath)      ;
    bool BuildFromString (std::string const& encodedstring) ;

    bool SerialiazeToFile         (std::string const& filepath);
    bool SerialiazeToOutputStream (std::ostream     & ostream) ;
    bool SerializeToString        (std::string      & ostring) ;

    std::string const ToString();

    void Clear();

protected:
    std::unique_ptr<google::protobuf::Message> m_message;

             StaticMessage() = default;
    virtual ~StaticMessage() = default;

    /*@***********************************************************************************
     * ::GetMessageAs : Returns the inner message derived from google::protobuf::Message *
     *                , casted to the desired derived generated one                      *
     *************************************************************************************/
    template
        <class DerivedMsg, typename = std::enable_if_t< std::is_base_of_v<google::protobuf::Message, DerivedMsg> > >
    inline
    DerivedMsg* GetMessageAs()
    {return static_cast<DerivedMsg*>(m_message.get());}

private:
    StaticMessage                   (StaticMessage const&)  = delete;
    StaticMessage                   (StaticMessage const&&) = delete;
    StaticMessage const&  operator= (StaticMessage const&)  = delete;
    StaticMessage const&& operator= (StaticMessage const&&) = delete;
};

