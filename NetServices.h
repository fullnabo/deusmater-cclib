#pragma once

/*$************************************************************************************************************************
 * \file   NetServices.h                                                                                                  *
 * \date   June 2019                                                                                                      *
 * \author Sofian Touhami                                                                                                 *
 *                                                                                                                        *
 * Contact: sofian.engineer@gmail.com                                                                                     *
 *                                                                                                                        *
 * \brief Asio network library, tcp connection management                                                                 *
 *                                                                                                                        *
 * The AsioService class statically initializes a context to avoid                                                        *
 * frequent static init fiasco with asio context object                                                                   *
 *   _________________________________________       ___________________________________________________________________  *
 *   |          Non blocking operation       |       |Blocking until every previous queued operations has been executed|  *
 *   |Start asynchronous routines/controllers|   >>  |                  Call Run() on the context                      |  *
 *   -----------------------------------------       -------------------------------------------------------------------  *
 *                       ^                                                   v                                            *
 *                       ^                                                   v                                            *
 *                       ^         _____________________________________________________________________________________  *
 *                       ^         |Keep attention here that : 1) the service is unlocked, but still available         |  *
 *                       ^         |                           2) the socket is still open                             |  *
 *                       ^         |Do whatever you want, but be careful if you do not clearly understand the mechanism|  *
 *                       ^         -------------------------------------------------------------------------------------  *
 *                       ^         v                                                                                      *
 *   _______________________________                                                                                      *
 *   |Call Restart() on the context|                                                                                      *
 *   -------------------------------                                                                                      *
 **************************************************************************************************************************/

#ifndef ASIO_STANDALONE
#define ASIO_STANDALONE
#endif
#include <asio.hpp>

#include <Utils.h>

#include <iostream>

/*!************************************
 * \class   AsioService               *
 * \author  Sofian Touhami            *
 * \version 1.0                       *
 * \date    June 2019                 *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 *                                    *
 **************************************/
class AsioService
{
public:
    static asio::io_service& Context();

    static void Run    ();
    static void Restart();

private:
    static asio::io_service m_iocontext;
};

/*!************************************
 * \class   NetSocket                 *
 * \author  Sofian Touhami            *
 * \version 1.0                       *
 * \date    June 2019                 *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 *                                    *
 **************************************/
class NetSocket
{
public:
    bool Read  ();
    bool Write (std::string const data);

    bool        HasInput   ();
    std::string GetInput   ();
    void        ResetInput ();

protected:
    NetSocket ();

protected:
    void handle_read  (std::error_code const& ec, size_t const& bytes);
    void handle_write (std::error_code const& ec, size_t const& bytes);

protected:
    asio::ip::tcp::socket  m_socket;

    std::array<char, 1024> m_input_buf  ;
    std::string            m_output_buf ;

    bool   m_has_input   = false ;
    size_t m_input_bytes = 0     ;
};

/*!************************************
 * \class   Server                    *
 * \author  Sofian Touhami            *
 * \version 1.0                       *
 * \date    June 2019                 *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 *                                    *
 **************************************/
class Server
    : public  NetSocket
    , private DisableCopy
{
public:
    Server(short const port);

    virtual ~Server() = default;

    virtual void Accept();

protected:
    void handle_accept(std::error_code const& ec);

protected:
    asio::ip::tcp::acceptor m_acceptor;
};

/*!************************************
 * \class   Client                    *
 * \author  Sofian Touhami            *
 * \version 1.0                       *
 * \date    June 2019                 *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 *                                    *
 **************************************/
class Client
    : public  NetSocket
    , private DisableCopy
{
public:
    Client(std::string const host, short const port);

    virtual ~Client() = default;

    virtual void Connect();

protected:
    void handle_connect(std::error_code const& ec);

protected:
    asio::ip::tcp::resolver::results_type m_endpoints;
};

