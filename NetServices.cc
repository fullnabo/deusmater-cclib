/*$************************************
 * \file   NetServices.cc             *
 * \date   June 2019                  *
 * \author Sofian Touhami             *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 **************************************/

#include <NetServices.h>
#include <Log.h>

/*!**************************************************
 * class AsioService                                *
 *     #::m_iocontext : Static initialization       *
 *     @::Context     : Returns the global service  *
 *     @::Run         : Runs the global service     *
 *     @::Restart     : Restarts the global service *
 ****************************************************/

/*#***************
 * ::m_iocontext *
 *****************/
asio::io_service AsioService::m_iocontext;

/*@***********
 * ::Context *
 *************/
asio::io_service& AsioService::Context()
{return m_iocontext;}

/*@*******
 * ::Run *
 *********/
void AsioService::Run()
{m_iocontext.run();}

/*@***********
 * ::Restart *
 *************/
void AsioService::Restart()
{m_iocontext.restart();}

/*!*******************************************************
 * class NetSocket                                       *
 *     @::Read         : Asynchronous read               *
 *     @::Write        : Asynchronous write              *
 *     @::handle_read  : Handler for async read          *
 *     @::handle_write : Handler for async write         *
 *     @::HasInput     : Flag for handle_read result     *
 *     @::GetInput     : Retrieves handle_read data      *
 *     @::ResetInput   : Reset the input buffer and flag *
 *********************************************************/

NetSocket::NetSocket()
    : m_socket(AsioService::Context())
{}

/*@********
 * ::Read *
 **********/
bool NetSocket::Read()
{
    if (false == m_socket.is_open())
        return false;

    m_socket.async_read_some(
        asio::buffer(m_input_buf)
        , std::bind(&NetSocket::handle_read, this, std::placeholders::_1, std::placeholders::_2)
    );

    return true;
}

/*@*********
 * ::Write *
 ***********/
bool NetSocket::Write(std::string const data)
{
    if(false == m_socket.is_open())
        return false;

    m_output_buf = data;

    m_socket.async_write_some(
        asio::buffer(m_output_buf)
        , std::bind(&NetSocket::handle_write, this, std::placeholders::_1, std::placeholders::_2)
    );

    return true;
}

/*@***************
 * ::handle_read *
 *****************/
void NetSocket::handle_read(std::error_code const& ec, size_t const& bytes)
{
    if(ec)
    {
        LockOut() << "handle_read : " << ec.message();
        return;
    }
    
    m_input_bytes = bytes ;
    m_has_input   = true  ;

    LockOut() << "handle_read (" << bytes << ") : " << GetInput();
}

/*@****************
 * ::handle_write *
 ******************/
void NetSocket::handle_write(std::error_code const& ec, size_t const& bytes)
{
    if(ec)
    {
        LockOut() << "handle_write : " << ec.message() << std::endl;
        return;
    }
    
    LockOut() << "handle_write (" << bytes << ") : " << m_output_buf << std::endl;
}

/*@************
 * ::HasInput *
 **************/
bool NetSocket::HasInput()
{return m_has_input;}

/*@************
 * ::GetInput *
 **************/
std::string NetSocket::GetInput()
{
    std::stringstream datastream;

    for (int i = 0; i < m_input_bytes; i++)
        datastream << m_input_buf.at(i);

    return datastream.str();
}

/*@**************
 * ::ResetInput *
 ****************/
void NetSocket::ResetInput()
{
    m_input_bytes = 0     ;
    m_has_input   = false ;
}

/*!*************************************************
 * class Server                                    *
 *     @::Accept        : Asynchronous accept      *
 *     @::handle_accept : Handler for async accept *
 ***************************************************/

Server::Server(short const port)
    : m_acceptor (AsioService::Context(), asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port))
{}

/*@**********
 * ::Accept *
 ************/
void Server::Accept()
{
    m_acceptor.async_accept
    (
        m_socket
        , std::bind(&Server::handle_accept, this, std::placeholders::_1)
    );
}

/*@*****************
 * ::handle_accept *
 *******************/
void Server::handle_accept(std::error_code const& ec)
{
    if(ec)
    {
        LockOut() << "handle_accept : " << ec.message() << std::endl;
        return;
    }

    LockOut() << "handle_accept : Accepted" << std::endl;
}

/*!***************************************************
 * class Client                                      *
 *     @::Connect        : Asynchronous connect      *
 *     @::handle_connect : Handler for async connect *
 *****************************************************/

Client::Client(std::string const host, short const port)
{
    asio::ip::tcp::resolver resolver(AsioService::Context());
    m_endpoints = resolver.resolve(host, std::to_string(port));
}

/*@***********
 * ::Connect *
 *************/
void Client::Connect()
{
    asio::async_connect(
        m_socket
        , m_endpoints
        , std::bind(&Client::handle_connect, this, std::placeholders::_1)
    );
}

/*@******************
 * ::handle_connect *
 ********************/
void Client::handle_connect(std::error_code const& ec)
{
    if(ec)
    {
        LockOut() << "handle_connect : " << ec.message() << std::endl;
        return;
    }

    LockOut() << "handle_connect : Connected" << std::endl;
}

