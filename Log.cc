/*$************************************
 * \file   Log.cc                     *
 * \date   June 2019                  *
 * \author Sofian Touhami             *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 **************************************/

#include <Log.h>
#include <iostream>
#include <mutex>

namespace
{
    std::mutex iomutex;
}

/*!*************************************
 * class LockOut                       *
 *     @::operator void : type casting *
 ***************************************/

LockOut::LockOut()
    : std::ostream(0)
    , m_os()
{
    this->init(m_os.rdbuf());
}

LockOut::~LockOut()
{
    std::lock_guard<std::mutex> lock(iomutex);

    m_os      << std::endl  ;
    std::cout << m_os.str() ;
}

/*@*****************
 * ::operator void *
 *******************/
LockOut::operator void()
{
    ;
}

