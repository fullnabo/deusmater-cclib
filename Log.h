#pragma once

/*$************************************
 * \file   Log.h                      *
 * \date   June 2019                  *
 * \author Sofian Touhami             *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 *                                    *
 * \brief Atomic output stream        *
 **************************************/

#include <sstream>

/*!************************************
 * \class   LockOut                   *
 * \author  Sofian Touhami            *
 * \version 1.0                       *
 * \date    June 2019                 *
 *                                    *
 * Contact: sofian.engineer@gmail.com *
 *                                    *
 **************************************/
class LockOut
    : public std::ostream
{
public:
     LockOut();
    ~LockOut();

    operator void();

private:
    std::ostringstream m_os;
};

